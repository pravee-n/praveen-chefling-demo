package net.chefling.praveendemo.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.chefling.praveendemo.R;
import net.chefling.praveendemo.viewHolders.RecipeTypeCardViewHolder;
import net.chefling.praveendemo.viewHolders.ServingCountCardViewHolder;

import org.json.JSONArray;

/**
 * Created by praveen on 28/10/16.
 */

public class GlobalRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private JSONArray data;
    private int type;

    public GlobalRecyclerViewAdapter(JSONArray data, int type) {
        this.data = data;
        this.type = type;
    }

    @Override
    public int getItemViewType(int position) {
        return this.type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        View view;
        switch(viewType) {
            case 0:
                view = inflater.inflate(R.layout.view_recipe_type_card, viewGroup, false);
                viewHolder = new RecipeTypeCardViewHolder(view);
                break;
            case 1:
                view = inflater.inflate(R.layout.view_serving_count_card, viewGroup, false);
                viewHolder = new ServingCountCardViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case 0:
                RecipeTypeCardViewHolder.fillView(holder, data.optString(position));
                break;
            case 1:
                ServingCountCardViewHolder.fillView(holder, data.optString(position));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}


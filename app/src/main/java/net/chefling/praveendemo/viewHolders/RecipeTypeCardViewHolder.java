package net.chefling.praveendemo.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import net.chefling.praveendemo.R;
import net.chefling.praveendemo.utils.RecipeSelectEvent;
import net.chefling.praveendemo.views.GlobalTextView;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by praveen on 28/10/16.
 */

public class RecipeTypeCardViewHolder extends RecyclerView.ViewHolder {

    GlobalTextView text;

    public RecipeTypeCardViewHolder(View view) {
        super(view);
        this.text = (GlobalTextView) view.findViewById(R.id.rtc_text);
    }

    public static void fillView(RecyclerView.ViewHolder holder, final String data) {
        final RecipeTypeCardViewHolder recipeTypeCardViewHolder = (RecipeTypeCardViewHolder) holder;
        recipeTypeCardViewHolder.text.setText(data);
        recipeTypeCardViewHolder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Post event that recipe type has been selected in dialog fragment
                EventBus.getDefault().post(new RecipeSelectEvent(recipeTypeCardViewHolder.text.getText().toString()));
            }
        });
    }
}

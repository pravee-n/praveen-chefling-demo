package net.chefling.praveendemo.utils;

/**
 * Created by praveen on 28/10/16.
 */

public class ServingCountSelectEvent {
    public final String message;

    public ServingCountSelectEvent(String message) {
        this.message = message;
    }
}

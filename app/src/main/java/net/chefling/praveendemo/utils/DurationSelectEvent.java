package net.chefling.praveendemo.utils;

/**
 * Created by praveen on 28/10/16.
 */

public class DurationSelectEvent {
    public final String message;

    public DurationSelectEvent(String message) {
        this.message = message;
    }
}

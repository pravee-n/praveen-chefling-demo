package net.chefling.praveendemo.utils;

import org.json.JSONObject;

/**
 * Created by praveen on 28/10/16.
 */

public interface BasicDataCallback {
    public void callback(JSONObject data);
}
package net.chefling.praveendemo.utils;

/**
 * Created by praveen on 28/10/16.
 */

public class RecipeSelectEvent {
    public final String message;

    public RecipeSelectEvent(String message) {
        this.message = message;
    }
}

package net.chefling.praveendemo.utils;

import android.content.Context;
import android.graphics.Typeface;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by praveen on 28/10/16.
 */

public class Utilities {

    public static Typeface fontFaceType;

    public static void setFontTypeFace(Context ctx) {
        fontFaceType = Typeface.createFromAsset( ctx.getAssets(), "Roboto-Medium.ttf" );
    }

    public static void getDataFromAPI(Context context, String URL, JSONObject params, final BasicDataCallback successCallback) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, URL, params, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        if (successCallback != null) {
                            successCallback.callback(response);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });

        VolleySingleton.getInstance(context).addToRequestQueue(jsObjRequest);
    }
}

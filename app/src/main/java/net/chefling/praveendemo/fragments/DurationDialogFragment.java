package net.chefling.praveendemo.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.NumberPicker;

import net.chefling.praveendemo.R;
import net.chefling.praveendemo.utils.DurationSelectEvent;
import net.chefling.praveendemo.utils.ServingCountSelectEvent;
import net.chefling.praveendemo.views.GlobalTextView;

import org.greenrobot.eventbus.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class DurationDialogFragment extends DialogFragment {

    View view;

    public DurationDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_duration_dialog, container, false);
        fillView();
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();

        // Set height and width of dialog
        int width = getResources().getDimensionPixelSize(R.dimen.duration_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.duration_dialog_height);
        getDialog().getWindow().setLayout(width, height);
        window.setGravity(Gravity.CENTER);
    }

    private void fillView() {
        final NumberPicker hourPicker = (NumberPicker) view.findViewById(R.id.dd_hour);
        final NumberPicker minPicker = (NumberPicker) view.findViewById(R.id.dd_min);
        GlobalTextView done = (GlobalTextView) view.findViewById(R.id.dd_done);
        GlobalTextView cancel = (GlobalTextView) view.findViewById(R.id.dd_cancel);

        hourPicker.setMaxValue(10);
        hourPicker.setMinValue(0);

        final String[] minValues = new String[12];
        for (int i = 0; i < minValues.length; i++) {
            String number = Integer.toString(i*5);
            minValues[i] = Integer.toString(i * 5);
            minValues[i] = number.length() < 2 ? "0" + number : number;
        }

        minPicker.setMinValue(0);
        minPicker.setMaxValue(11);
        minPicker.setDisplayedValues(minValues);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
                String message = String.valueOf(hourPicker.getValue()) + "h " + String.valueOf(minValues[minPicker.getValue()]) + "m";

                // Post event that duration has been selected
                EventBus.getDefault().post(new DurationSelectEvent(message));
            }
        });
    }

}

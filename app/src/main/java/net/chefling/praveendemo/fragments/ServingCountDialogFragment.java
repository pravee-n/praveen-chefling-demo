package net.chefling.praveendemo.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import net.chefling.praveendemo.R;
import net.chefling.praveendemo.adapters.GlobalRecyclerViewAdapter;
import net.chefling.praveendemo.views.GlobalRecyclerView;

import org.json.JSONArray;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServingCountDialogFragment extends DialogFragment {

    View view;

    public ServingCountDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_serving_count_dialog, container, false);
        fillView();
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void onResume() {
        super.onResume();

        // Set height and width of dialog
        Window window = getDialog().getWindow();
        int width = getResources().getDimensionPixelSize(R.dimen.serving_count_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.serving_count_dialog_height);
        getDialog().getWindow().setLayout(width, height);
        window.setGravity(Gravity.CENTER);
    }

    private void fillView() {
        JSONArray dataArray = new JSONArray();
        for (int i = 1; i <= 10; i++) {
            if (i == 10) {
                dataArray.put(i + "+");
            }
            else {
                dataArray.put(i);
            }
        }

        GlobalRecyclerView recyclerView = (GlobalRecyclerView) view.findViewById(R.id.scd_rv);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        GlobalRecyclerViewAdapter mAdapter = new GlobalRecyclerViewAdapter(dataArray, 1);
        recyclerView.setAdapter(mAdapter);
    }

}

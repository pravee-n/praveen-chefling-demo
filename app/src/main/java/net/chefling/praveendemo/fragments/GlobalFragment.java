package net.chefling.praveendemo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.chefling.praveendemo.R;

/**
 * A simple {@link Fragment} subclass.
 */



/*
 * Simple class that extends fragment, so that if we have to add a feature
 * to all fragments, simply add it here and extend all fragments to
 * this GlobalFragment Class
 */


public class GlobalFragment extends Fragment {


    public GlobalFragment() {
        // Required empty public constructor
    }

}

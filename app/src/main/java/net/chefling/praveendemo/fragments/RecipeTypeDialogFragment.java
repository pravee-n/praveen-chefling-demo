package net.chefling.praveendemo.fragments;


import android.app.Dialog;
import android.icu.util.ValueIterator;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.android.volley.Request;

import net.chefling.praveendemo.R;
import net.chefling.praveendemo.adapters.GlobalRecyclerViewAdapter;
import net.chefling.praveendemo.utils.BasicDataCallback;
import net.chefling.praveendemo.utils.RecipeSelectEvent;
import net.chefling.praveendemo.utils.Urls;
import net.chefling.praveendemo.utils.Utilities;
import net.chefling.praveendemo.views.GlobalRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeTypeDialogFragment extends DialogFragment {

    View view;

    public RecipeTypeDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_recipe_type_dialog, container, false);
        getData();
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        int width = getResources().getDimensionPixelSize(R.dimen.recipe_type_dialog_width);
        int height = getResources().getDimensionPixelSize(R.dimen.recipe_type_dialog_height);
        getDialog().getWindow().setLayout(width, height);
        window.setGravity(Gravity.CENTER);
    }

    private void getData() {
        BasicDataCallback recipeDataCallback = new BasicDataCallback() {
            @Override
            public void callback(JSONObject data) {
                fillRows(data);
            }
        };
        JSONObject params = new JSONObject();
        try {
            params.put("RTid", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utilities.getDataFromAPI(getActivity().getApplicationContext(), Urls.recipeData, params, recipeDataCallback);
    }

    private void fillRows(JSONObject data) {
        JSONArray dataArray = new JSONArray();
        Iterator<?> keys = data.keys();

        while( keys.hasNext() ) {
            String key = (String)keys.next();
            dataArray.put(data.optString(key));
        }
        GlobalRecyclerView recyclerView = (GlobalRecyclerView) view.findViewById(R.id.rtd_rv);
        RelativeLayout loader = (RelativeLayout) view.findViewById(R.id.rtd_loader);
        loader.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        GlobalRecyclerViewAdapter mAdapter = new GlobalRecyclerViewAdapter(dataArray, 0);
        recyclerView.setAdapter(mAdapter);
    }

}

package net.chefling.praveendemo.fragments;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.ImagePickerSheetView;

import net.chefling.praveendemo.BlurImage;
import net.chefling.praveendemo.R;
import net.chefling.praveendemo.utils.DurationSelectEvent;
import net.chefling.praveendemo.utils.RecipeSelectEvent;
import net.chefling.praveendemo.utils.ServingCountSelectEvent;
import net.chefling.praveendemo.views.GlobalEditText;
import net.chefling.praveendemo.views.GlobalTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecipeFragment extends GlobalFragment {

    View view;
    RecipeTypeDialogFragment recipeTypeDialogFragment;
    ServingCountDialogFragment servingCountDialogFragment;
    DurationDialogFragment durationDialogFragment;
    GlobalEditText recipeType;
    protected BottomSheetLayout bottomSheetLayout;
    private Uri cameraImageUri = null;
    ImageView mainImgView;
    private static final int REQ_STORAGE = 0;
    private static final int REQ_IMG_CAPTURE = 1;
    private static final int REQ_LOAD_IMG = 2;

    public RecipeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recipe, container, false);
        fillView();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void fillView() {
        fillImageSection();
        fillDifficultyBtns();
        fillServingSection();
        fillRecipeTypeSection();
        fillCookingTimeSection();
        fillNextButtonSection();
    }

    /* Fill top image section and bind click events */
    private void fillImageSection() {
        RelativeLayout imageContainer = (RelativeLayout) view.findViewById(R.id.rf_img_container);
        bottomSheetLayout = (BottomSheetLayout) view.findViewById(R.id.bottomsheet);
        mainImgView = (ImageView) imageContainer.findViewById(R.id.rf_img);

        final ViewTreeObserver observer = mainImgView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ViewTreeObserver vto = mainImgView.getViewTreeObserver();
                if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    vto.removeGlobalOnLayoutListener(this);
                } else {
                    vto.removeOnGlobalLayoutListener(this);
                }
                blurMainImg(null);
            }
        });

        bottomSheetLayout.setPeekOnDismiss(true);

        imageContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (permissionNotGranted()) {
                    requestPermissions();
                } else {
                    showSheetView();
                }
            }
        });
    }

    /* Fill difficulty level buttons section */
    private void fillDifficultyBtns() {
        final List <GlobalTextView> btnsList = new ArrayList<>();
        btnsList.add((GlobalTextView) view.findViewById(R.id.rf_difficulty_btn_1));
        btnsList.add((GlobalTextView) view.findViewById(R.id.rf_difficulty_btn_2));
        btnsList.add((GlobalTextView) view.findViewById(R.id.rf_difficulty_btn_3));

        for (int i = 0; i < btnsList.size(); i++) {
            final int finalI = i;
            btnsList.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int j = 0; j < btnsList.size(); j++) {
                        if (finalI != j) {
                            btnsList.get(j).setTextColor(ContextCompat.getColor(getContext(), R.color.black_33));
                            btnsList.get(j).setBackgroundResource(R.drawable.warm_grey_1dp_border_8dp_radius);
                        }
                        else {
                            btnsList.get(j).setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                            btnsList.get(j).setBackgroundResource(R.drawable.green_85_bg_8dp_corner);
                        }
                    }
                }
            });
        }
    }

    /* Fill serving button section */
    private void fillServingSection() {
        LinearLayout serveBtn = (LinearLayout) view.findViewById(R.id.rf_serve_btn);
        // Open serving count dialog
        serveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                servingCountDialogFragment = new ServingCountDialogFragment();
                servingCountDialogFragment.show(fm, "fragment_serving_count");
            }
        });

    }

    /* Fill recipe type button section */
    private void fillRecipeTypeSection() {
        recipeType = (GlobalEditText) view.findViewById(R.id.rf_recipe_type);
        // Open crecipe type selection dialog
        recipeType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                recipeTypeDialogFragment = new RecipeTypeDialogFragment();
                recipeTypeDialogFragment.show(fm, "fragment_recipe_type");
            }
        });
    }

    /* Fill cooking time button section */
    private void fillCookingTimeSection() {
        LinearLayout cookingTimeBtn = (LinearLayout) view.findViewById(R.id.rf_cooking_time_btn);
        // Open cooking time selection dialog
        cookingTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                durationDialogFragment = new DurationDialogFragment();
                durationDialogFragment.show(fm, "fragment_duration");
            }
        });
    }

    /* Fill next button section */
    private void fillNextButtonSection() {
        LinearLayout nextButton = (LinearLayout) view.findViewById(R.id.rf_next_btn);
        final Vibrator vibe = (Vibrator) getActivity().getSystemService(getContext().VIBRATOR_SERVICE);
        // Give some feedback when next is clicked e.g vibrate for 100ms
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibe.vibrate(100);
            }
        });
    }

    /* Function that sets a blurred bitmap to main imageView
    * Either takes a bitmap as a parameter, or reads the bitmap
    * from main imageView
    * */
    private void blurMainImg(Bitmap originalBitmap) {
        // Get bitmap from camera image or image picked from gallery
        if (originalBitmap == null) {
            // If Bitmap is null, get bitmap from imageView
            originalBitmap = ((BitmapDrawable) mainImgView.getDrawable()).getBitmap();
        }
        Bitmap blurredBitmap = BlurImage.blur(getContext(), originalBitmap);
        mainImgView.setImageDrawable(new BitmapDrawable( getResources(), blurredBitmap ));
    }

    /* Receive recipe selected event from dialog fragment */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRecipeSelectEvent(RecipeSelectEvent event) {
        recipeTypeDialogFragment.dismiss();
        recipeType.setText(event.message);
    }

    /* Receive serving count selected event from dialog fragment */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServingCountSelectEvent(ServingCountSelectEvent event) {
        servingCountDialogFragment.dismiss();
        GlobalTextView servingCountText = (GlobalTextView) view.findViewById(R.id.rf_serve_text);
        servingCountText.setText("Serves: " + event.message);
    }

    /* Receive duration selected event from dialog fragment */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDurationSelectEvent(DurationSelectEvent event) {
        GlobalTextView cookingTimeText = (GlobalTextView) view.findViewById(R.id.rf_cooking_text);
        cookingTimeText.setText(event.message);
    }

    private void showSheetView() {
        ImagePickerSheetView sheetView = new ImagePickerSheetView.Builder(getContext())
                .setMaxItems(30)
                .setShowCameraOption(createCameraIntent() != null)
                .setShowPickerOption(createPickImageIntent() != null)
                .setImageProvider(new ImagePickerSheetView.ImageProvider() {
                    @Override
                    public void onProvideImage(ImageView imageView, Uri imageUri, int size) {
                        Glide.with(getContext())
                                .load(imageUri)
                                .centerCrop()
                                .crossFade()
                                .into(imageView);
                    }
                })
                .setOnTileSelectedListener(new ImagePickerSheetView.OnTileSelectedListener() {
                    @Override
                    public void onTileSelected(ImagePickerSheetView.ImagePickerTile selectedTile) {
                        bottomSheetLayout.dismissSheet();
                        if (selectedTile.isCameraTile()) {
                            getImageFromCamera();
                        } else if (selectedTile.isPickerTile()) {
                            startActivityForResult(createPickImageIntent(), REQ_LOAD_IMG);
                        } else if (selectedTile.isImageTile()) {
                            showSelectedImage(selectedTile.getImageUri());
                        } else {
                            showImageError(null);
                        }
                    }
                })
                .setTitle("Choose an image...")
                .create();

        bottomSheetLayout.showWithSheetView(sheetView);
    }

    private Intent createCameraIntent() {
        Intent cameraImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraImageIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            return cameraImageIntent;
        } else {
            return null;
        }
    }

    private Intent createPickImageIntent() {
        Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (pickImageIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            return pickImageIntent;
        } else {
            return null;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQ_STORAGE);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQ_STORAGE);
        }
    }

    private boolean permissionNotGranted() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri selectedImage = null;
            if (requestCode == REQ_LOAD_IMG && data != null) {
                selectedImage = data.getData();
                if (selectedImage == null) {
                    showImageError(null);
                }
            } else if (requestCode == REQ_IMG_CAPTURE) {
                // Do something with imagePath
                selectedImage = cameraImageUri;
            }

            if (selectedImage != null) {
                showSelectedImage(selectedImage);
            } else {
                showImageError(null);
            }
        }
    }

    private void showSelectedImage(Uri selectedImageUri) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainImgView.setImageDrawable(null);
        blurMainImg(bitmap);
    }

    private void getImageFromCamera() {
        Intent cameraImageIntent = createCameraIntent();
        // Ensure that there's a camera activity to handle the intent
        if (cameraImageIntent != null) {
            // Create the File where the photo should go
            try {
                File imageFile = createImageFile();
                cameraImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                startActivityForResult(cameraImageIntent, REQ_IMG_CAPTURE);
            } catch (IOException e) {
                // Error occurred while creating the File
                showImageError("Could not create imageFile for camera");
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        cameraImageUri = Uri.fromFile(imageFile);
        return imageFile;
    }

    private void showImageError(String message) {
        Toast.makeText(getContext(), message == null ? "Something went wrong." : message, Toast.LENGTH_SHORT).show();
    }

}

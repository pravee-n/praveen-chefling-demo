package net.chefling.praveendemo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import net.chefling.praveendemo.utils.Utilities;

/**
 * Created by praveen on 27/10/16.
 */

public class GlobalTextView extends TextView {
    public GlobalTextView(Context context) {
        super(context);
        init();
    }

    public GlobalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GlobalTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init () {
        this.setTypeface(Utilities.fontFaceType);
    }
}

package net.chefling.praveendemo.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by praveen on 27/10/16.
 */

public class GlobalEditText extends EditText {
    public GlobalEditText(Context context) {
        super(context);
    }

    public GlobalEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GlobalEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}

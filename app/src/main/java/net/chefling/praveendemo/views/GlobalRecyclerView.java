package net.chefling.praveendemo.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by praveen on 28/10/16.
 */

public class GlobalRecyclerView extends RecyclerView {
    public GlobalRecyclerView(Context context) {
        super(context);
    }

    public GlobalRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public GlobalRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
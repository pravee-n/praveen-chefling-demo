package net.chefling.praveendemo.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import net.chefling.praveendemo.R;
import net.chefling.praveendemo.utils.Utilities;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bootApp();
        setContentView(R.layout.activity_main);
    }

    private void bootApp() {
        Utilities.setFontTypeFace(this);
    }
}
